import com.sun.deploy.util.ArrayUtil;

import java.util.Arrays;

public class Task2 {

    private static final int NOT_FOUND = -1;
    private static final int MAX_DISTANCE = 100000000;

    public int solution(int[] A) {

        Arrays.sort(A);

        int minDistance = 0;

        for (int i = 0; i < A.length; i++) {

            int currentDistance = Math.abs(A[i] - A[i + 1]);

            if (currentDistance < minDistance) {
                minDistance = currentDistance;
            }
        }

        return minDistance > MAX_DISTANCE
                ? NOT_FOUND
                : minDistance;
    }

}
