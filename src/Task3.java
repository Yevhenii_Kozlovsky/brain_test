import java.util.HashSet;
import java.util.Set;

public class Task3 {

    private static final int LAST_STOP = 1;

    public int solution(int[] peopleFloors,
                        int[] peopleWeights,
                        int maxFloor,
                        int elevatorCapacity,
                        int elevatorWeight) {

        int currentCapacity = 0;
        int currentWeight = 0;
        int elevatorStops = 0;
        Set<Integer> raceFloors = new HashSet<>(maxFloor);

        for (int personNumber = 0; personNumber < peopleFloors.length; personNumber++) {

            if (currentCapacity + 1 >= elevatorCapacity
                    || currentWeight + peopleWeights[personNumber] >= elevatorWeight) {

                currentCapacity = currentWeight = 0;
                elevatorStops = raceFloors.size() + LAST_STOP;
                raceFloors.clear();
            } else {
                raceFloors.add(peopleWeights[personNumber]);
            }

            currentCapacity++;
            currentWeight += peopleWeights[personNumber];
        }

        return elevatorStops;
    }

}
