
public class Task1 {

    private static final int NOT_POSSIBLE = -1;

    public int solution(int[] A, int steps) {

        boolean[] stones = new boolean[steps + 1];

        for (int i = 0; i < A.length; i++) {

            if (!stones[A[i]]) {
                stones[A[i]] = true;
                steps--;
            }

            if (steps == 0) {
                return i;
            }
        }

        return NOT_POSSIBLE;
    }

}
